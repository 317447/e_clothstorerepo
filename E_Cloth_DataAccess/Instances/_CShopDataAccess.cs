﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Cloth_DataAccess
{
    public partial class _CShopDataAccess
    {
        #region Private
        private static _CShopDataAccess Instance = new _CShopDataAccess();
        public static _CShopDataAccess GetInstances
        {
            get
            {
                return Instance;
            }
        }
        private _CShopDataAccess()
        {

        }
        #endregion
    }
}