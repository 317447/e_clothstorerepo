﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Cloth_DataAccess
{
    public partial class _CShopDataAccess
    {
        E_Cloth_StoreEntities contextt = new E_Cloth_StoreEntities();
        public List<TBL_BRAND_MST> BrndViewBindDA()
        {
            var data = (from m in contextt.TBL_BRAND_MST
                        select m).ToList();
            return data;
        }
        public TBL_BRAND_MST InsertBrndMst(TBL_BRAND_MST brnd)
        {
            contextt.TBL_BRAND_MST.Add(brnd);
            contextt.SaveChanges();
            return brnd;
        }
        public TBL_BRAND_MST UpdateBrndMst(TBL_BRAND_MST brnd, int? brndID)
        {
            TBL_BRAND_MST BrndUpdate = contextt.TBL_BRAND_MST.First(t => t.BR_ID == brndID);
            BrndUpdate.BR_NAME = brnd.BR_NAME;
            BrndUpdate.BR_DESC = brnd.BR_DESC;
            BrndUpdate.ITM_ID = brnd.ITM_ID;
            BrndUpdate.IS_ACTIVE = true;
            BrndUpdate.IS_DELETE = false;
            contextt.SaveChanges();
            return brnd;
        }
        public TBL_BRAND_MST DeleteBrndMst(TBL_BRAND_MST brnd, int? brndID)
        {
            TBL_BRAND_MST BrndDelete = contextt.TBL_BRAND_MST.Find(brndID);
            BrndDelete.IS_ACTIVE = false;
            BrndDelete.IS_DELETE = true;
            return brnd;
        }
    }
}