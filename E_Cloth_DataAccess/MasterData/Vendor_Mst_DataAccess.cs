﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Cloth_DataAccess
{
    public partial class _CShopDataAccess
    {
        E_Cloth_StoreEntities contextVendor = new E_Cloth_StoreEntities();
        public List<TBL_VENDOR_MST> VendorViewBindDA()
        {
            var data = (from m in contextVendor.TBL_VENDOR_MST
                        select m).ToList();
            return data;
        }
        public TBL_VENDOR_MST InsertVendorMst(TBL_VENDOR_MST vendor)
        {
            contextVendor.TBL_VENDOR_MST.Add(vendor);
            contextVendor.SaveChanges();
            return vendor;
        }
        public TBL_VENDOR_MST UpdateVendorMst(TBL_VENDOR_MST vendor, int? vendorID)
        {
            TBL_VENDOR_MST VendorUpdate = contextVendor.TBL_VENDOR_MST.First(t => t.VEN_ID == vendorID);
            VendorUpdate.VEN_ID = vendor.VEN_ID;
            VendorUpdate.VEN_NAME = vendor.VEN_NAME;
            //VendorUpdate.VEN_CODE = vendor.VEN_CODE;
            VendorUpdate.VEN_MOB = vendor.VEN_MOB;
            VendorUpdate.VEN_EMAIL = vendor.VEN_EMAIL;
            VendorUpdate.VEN_ADDRESS = vendor.VEN_ADDRESS;
            VendorUpdate.MODIFIED_ON = vendor.MODIFIED_ON;
            VendorUpdate.MODIFIED_BY = vendor.MODIFIED_BY;
            VendorUpdate.IS_ACTIVE = true;
            VendorUpdate.IS_DELETE = false;
            context.SaveChanges();
            return vendor;
        }
        public TBL_VENDOR_MST DeleteVendorMst(TBL_VENDOR_MST vendor, int? vendorID)
        {
            TBL_VENDOR_MST VendorDelete = context.TBL_VENDOR_MST.Find(vendorID);
            VendorDelete.IS_ACTIVE = false;
            VendorDelete.IS_DELETE = true;

            return vendor;
        }
    }
}