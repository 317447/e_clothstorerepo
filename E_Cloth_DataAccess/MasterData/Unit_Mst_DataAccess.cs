﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Cloth_DataAccess
{
    public partial class _CShopDataAccess
    {
        E_Cloth_StoreEntities contextUnit = new E_Cloth_StoreEntities();
        public List<TBL_UNIT_MST> UnitViewBindDA()
        {
            var data = (from m in contextUnit.TBL_UNIT_MST
                        select m).ToList();
            return data;
        }
        public TBL_UNIT_MST InsertUnitMst(TBL_UNIT_MST unit)
        {
            context.TBL_UNIT_MST.Add(unit);
            context.SaveChanges();
            return unit;
        }
        public TBL_UNIT_MST UpdateUnitMst(TBL_UNIT_MST unit, int? unitID)
        {
            TBL_UNIT_MST UnitUpdate = contextUnit.TBL_UNIT_MST.First(t => t.UNT_ID == unitID);
            UnitUpdate.UNT_NAME = unit.UNT_NAME;
            UnitUpdate.UNT_DESC = unit.UNT_DESC;
            UnitUpdate.MODIFIED_BY = unit.MODIFIED_BY;
            UnitUpdate.MODIFIED_ON = unit.MODIFIED_ON;
            UnitUpdate.IS_ACTIVE = true;
            UnitUpdate.IS_DELETE = false;
            context.SaveChanges();
            return unit;
        }
        public TBL_UNIT_MST DeleteunitMst(TBL_UNIT_MST unit, int? unitID)
        {
            TBL_UNIT_MST UnitDelete = contextUnit.TBL_UNIT_MST.Find(unitID);
            UnitDelete.IS_ACTIVE = false;
            UnitDelete.IS_DELETE = true;

            return unit;
        }
    }
}