﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Cloth_DataAccess
{
    public partial class _CShopDataAccess
    {
        E_Cloth_StoreEntities contextTax = new E_Cloth_StoreEntities();
        public List<TBL_TAX_MST> TaxViewBindDA()
        {
            var data = (from m in contextTax.TBL_TAX_MST
                        select m).ToList();
            return data;
        }
        public TBL_TAX_MST InsertTaxMst(TBL_TAX_MST tax)
        {
            contextTax.TBL_TAX_MST.Add(tax);
            contextTax.SaveChanges();
            return tax;
        }
        public TBL_TAX_MST UpdateTaxMst(TBL_TAX_MST tax, int? taxID)
        {
            TBL_TAX_MST TaxUpdate = contextTax.TBL_TAX_MST.First(t => t.TAX_ID == taxID);
            TaxUpdate.TAX_DESC = tax.TAX_DESC;
            TaxUpdate.TAX_RATE = tax.TAX_RATE;
            TaxUpdate.MODIFIED_ON = tax.MODIFIED_ON;
            TaxUpdate.MODIFIED_BY = tax.MODIFIED_BY;
            TaxUpdate.IS_ACTIVE = true;
            TaxUpdate.IS_DELETE = false;
            contextTax.SaveChanges();
            return tax;
        }
        public TBL_TAX_MST DeleteTaxMst(TBL_TAX_MST tax, int? taxID)
        {
            TBL_TAX_MST TaxDelete = contextTax.TBL_TAX_MST.Find(taxID);
            TaxDelete.IS_ACTIVE = false;
            TaxDelete.IS_DELETE = true;

            return tax;
        }
    }
}