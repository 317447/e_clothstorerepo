﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Cloth_DataAccess
{
    public partial class _CShopDataAccess
    {
        E_Cloth_StoreEntities context = new E_Cloth_StoreEntities();
        public List<TBL_ITEM_MST> ItmViewBindDA()
        {
            var data = (from m in context.TBL_ITEM_MST
                        select m).ToList();
            return data;
        }
        public TBL_ITEM_MST InsertItemMst(TBL_ITEM_MST item)
        {
            context.TBL_ITEM_MST.Add(item);
            context.SaveChanges();
            return item;
        }
        public TBL_ITEM_MST UpdateItemMst(TBL_ITEM_MST item, int? itmID)
        {
            TBL_ITEM_MST ItemUpdate = context.TBL_ITEM_MST.First(t => t.ITM_ID == itmID);
            ItemUpdate.ITM_ID = item.ITM_ID;
            ItemUpdate.ITM_NAME = item.ITM_NAME;
            ItemUpdate.ITM_DESC = item.ITM_DESC;
            ItemUpdate.IS_ACTIVE = true;
            ItemUpdate.IS_DELETE = false;
            context.SaveChanges();
            return item;
        }
        public TBL_ITEM_MST DeleteItemMst(TBL_ITEM_MST item, int? itemID)
        {
            TBL_ITEM_MST ItemDelete = context.TBL_ITEM_MST.Find(itemID);
            ItemDelete.IS_ACTIVE = false;
            ItemDelete.IS_DELETE = true;
            return item;
        }
    }
}