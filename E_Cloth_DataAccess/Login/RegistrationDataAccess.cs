﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Collections;

namespace E_Cloth_DataAccess
{
    public partial class _CShopDataAccess
    {
        E_Cloth_StoreEntities storeEntity = new E_Cloth_StoreEntities();
        public List<TBL_REGD_MST> GetRegd()
        {
            var data = (from a in storeEntity.TBL_REGD_MST select a).ToList();
            return data;
        }
        //---Save Data---\\
        public TBL_REGD_MST SaveRegd(TBL_REGD_MST regd)
        {
            storeEntity.TBL_REGD_MST.Add(regd);
            storeEntity.SaveChanges();
            return regd;
        }
    }
}