﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Cloth_Business
{
    public partial class _CShopManagement
    {
        private static _CShopManagement Instance;
        public static _CShopManagement GetInstance
        {
            get
            {
                if (Instance == null)
                {
                    Instance = new _CShopManagement();
                }
                return Instance;
            }
            set
            {
                Instance = value;
            }
        }

        private _CShopManagement() { }
    }
}