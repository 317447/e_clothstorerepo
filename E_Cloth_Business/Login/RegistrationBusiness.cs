﻿using E_Cloth_DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Cloth_Business
{
    public partial class  _CShopManagement
    {
        public List<TBL_REGD_MST> GetRegdDet()
        {
            return _CShopDataAccess.GetInstances.GetRegd();
        }
        public TBL_REGD_MST SaveRegdDet(TBL_REGD_MST regd)
        {
           return _CShopDataAccess.GetInstances.SaveRegd(regd);
        }
    }
}