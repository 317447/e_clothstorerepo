﻿using E_Cloth_DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Cloth_Business
{
    public partial class _CShopManagement
    {
        public List<TBL_VENDOR_MST> VendorViewBindBA()
        {
            return _CShopDataAccess.GetInstances.VendorViewBindDA();
        }
        public TBL_VENDOR_MST InsertVendor(TBL_VENDOR_MST vendor)
        {
            return _CShopDataAccess.GetInstances.InsertVendorMst(vendor);
        }
        public TBL_VENDOR_MST UpdateVendor(TBL_VENDOR_MST vendor, int? VendorID)
        {
            return _CShopDataAccess.GetInstances.UpdateVendorMst(vendor, VendorID);
        }
        public TBL_VENDOR_MST DeleteVendor(TBL_VENDOR_MST vendor, int? VendorID)
        {
            return _CShopDataAccess.GetInstances.DeleteVendorMst(vendor, VendorID);
        }
    }
}