﻿using E_Cloth_DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Cloth_Business
{
    public partial class _CShopManagement
    {
        public List<TBL_TAX_MST> TaxViewBindBA()
        {
            return _CShopDataAccess.GetInstances.TaxViewBindDA();
        }
        public TBL_TAX_MST InsertTax(TBL_TAX_MST tax)
        {
            return _CShopDataAccess.GetInstances.InsertTaxMst(tax);
        }
        public TBL_TAX_MST UpdateTax(TBL_TAX_MST tax, int? TaxID)
        {
            return _CShopDataAccess.GetInstances.UpdateTaxMst(tax, TaxID);
        }
        public TBL_TAX_MST DeleteTax(TBL_TAX_MST tax, int? TaxID)
        {
            return _CShopDataAccess.GetInstances.DeleteTaxMst(tax, TaxID);
        }
    }
}