﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading.Tasks;
using E_Cloth_DataAccess;


namespace E_Cloth_Business
{
    public partial class _CShopManagement
    {
        public List<TBL_BRAND_MST> BrndViewBindBA()
        {
            return _CShopDataAccess.GetInstances.BrndViewBindDA();
        }
        public TBL_BRAND_MST InsertBrand(TBL_BRAND_MST brand)
        {
            return _CShopDataAccess.GetInstances.InsertBrndMst(brand);
        }
       public TBL_BRAND_MST UpdateBrand(TBL_BRAND_MST brand, int?brandID)
        {
            return _CShopDataAccess.GetInstances.UpdateBrndMst(brand, brandID);
        }
        public TBL_BRAND_MST DeleteBrand(TBL_BRAND_MST brand, int?brandID)
       {
           return _CShopDataAccess.GetInstances.DeleteBrndMst(brand, brandID);
       }
    }
}