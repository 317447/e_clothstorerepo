﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading.Tasks;
using E_Cloth_DataAccess;

namespace E_Cloth_Business
{
    public partial class _CShopManagement
    {
        public List<TBL_ITEM_MST> ItmViewBindBA()
        {
            return _CShopDataAccess.GetInstances.ItmViewBindDA();
        }
        public TBL_ITEM_MST InsertItem(TBL_ITEM_MST item)
        {
            return _CShopDataAccess.GetInstances.InsertItemMst(item);
        }
        public TBL_ITEM_MST UpdateItem(TBL_ITEM_MST item, int? ItemID)
        {
            return _CShopDataAccess.GetInstances.UpdateItemMst(item, ItemID);
        }
        public TBL_ITEM_MST DeleteItem(TBL_ITEM_MST item, int? ItemID)
        {
            return _CShopDataAccess.GetInstances.DeleteItemMst(item, ItemID);
        }
    }
}