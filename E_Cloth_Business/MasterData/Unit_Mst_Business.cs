﻿using E_Cloth_DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Cloth_Business
{
    public partial class _CShopManagement
    {
        public List<TBL_UNIT_MST> UnitViewBindBA()
        {
            return _CShopDataAccess.GetInstances.UnitViewBindDA();
        }
        public TBL_UNIT_MST InsertUnit(TBL_UNIT_MST unit)
        {
            return _CShopDataAccess.GetInstances.InsertUnitMst(unit);
        }
        public TBL_UNIT_MST UpdateUnit(TBL_UNIT_MST unit, int UnitID)
        {
            return _CShopDataAccess.GetInstances.UpdateUnitMst(unit, UnitID);
        }
        public TBL_UNIT_MST DeleteUnit(TBL_UNIT_MST unit, int? unitID)
        {
            return _CShopDataAccess.GetInstances.DeleteunitMst(unit, unitID);
        }
    }
}