﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Cloth_Store.Model
{
    public class Unit_Mst_Class
    {
        public int UNT_ID { get; set; }
        public string UNT_NAME { get; set; }
        public string UNT_DESC { get; set; }
        public Nullable<int> CREATED_BY { get; set; }
        public Nullable<System.DateTime> CREATED_ON { get; set; }
        public Nullable<int> MODIFIED_BY { get; set; }
        public Nullable<System.DateTime> MODIFIED_ON { get; set; }
        public Nullable<bool> IS_ACTIVE { get; set; }
        public Nullable<bool> IS_DELETE { get; set; }
    }
}