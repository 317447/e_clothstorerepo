﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Cloth_Store.Model
{
    public class Vendor_Mst_Class
    {
        public int VEN_ID { get; set; }
        public string VEN_CODE { get; set; }
        public string VEN_NAME { get; set; }
        public string VEN_MOB { get; set; }
        public string VEN_EMAIL { get; set; }
        public string VEN_ADDRESS { get; set; }
        public Nullable<int> CREATED_BY { get; set; }
        public Nullable<System.DateTime> CREATED_ON { get; set; }
        public Nullable<int> MODIFIED_BY { get; set; }
        public Nullable<System.DateTime> MODIFIED_ON { get; set; }
        public Nullable<bool> IS_ACTIVE { get; set; }
        public Nullable<bool> IS_DELETE { get; set; }
       }
}