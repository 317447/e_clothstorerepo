﻿using E_Cloth_Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace E_Cloth_Store.Master
{
    public partial class _ClothStoreMaster : System.Web.UI.MasterPage
    {
        string role;
        string email;
        string id;
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpCookie aCookie = Request.Cookies["role"];
            //role = Server.HtmlEncode(aCookie.Value);
            //HttpCookie aCookie1 = Request.Cookies["email"];
            //email = Server.HtmlEncode(aCookie1.Value);
            //HttpCookie aCookie2 = Request.Cookies["id"];
            //id = Server.HtmlEncode(aCookie2.Value);
            if (!Page.IsPostBack)
            {
                //GetLoginName();
                visibleLink();
            }

            //HttpCookie aCookie = Request.Cookies["role"];
            //role = Server.HtmlEncode(aCookie.Value);
            //HttpCookie aCookie1 = Request.Cookies["email"];
            //email = Server.HtmlEncode(aCookie1.Value);
            //HttpCookie aCookie2 = Request.Cookies["id"];
            //id = Server.HtmlEncode(aCookie2.Value);
            //if (!Page.IsPostBack)
            //{
            //    GetLoginName();
            //    visibleLink();
            //}

        }
        protected void GetLoginName()
        {
            var regd = _CShopManagement.GetInstance.GetRegdDet().ToList();
            var data = (from a in regd where a.REG_EMAIL == email select a).ToList();

            lblName.Text = data[0].REG_NAME;
            lblhName.Text = data[0].REG_NAME;
            //lbltName.Text = data[0].REG_ACTIVITY;

        }
        protected void visibleLink()
        {

            if (role == "Admin")
            {
                permission.Visible = true;
            }
            else
            {
                permission.Visible = false;
            }
        }
        protected void home_click(object sender, EventArgs e)
        {
            if (role == "Admin")
            {
                Response.Redirect("../Admin/AdminDashBoard.aspx");
            }
            else
            {
                Response.Redirect("../User/UserDashBoard.aspx");
            }
        }
    }
}