﻿using E_Cloth_Store.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace E_Cloth_Store.MasterData
{
    public partial class Vendor_Mst : System.Web.UI.Page
    {
        int id = 1;
        List<Vendor_Mst_Class> VendorMstList = null;
        Vendor_Mst_Class vendorMstEntity = new Vendor_Mst_Class();
        HttpClient client = new HttpClient();
        HttpResponseMessage hrm = null;

        string charachter;
        int count;
        protected void Page_Load(object sender, EventArgs e)
        {
            var appsurl = ConfigurationManager.AppSettings["APIServiceURL"].ToString();
            client.BaseAddress = new Uri(appsurl);
            if (!Page.IsPostBack)
            {
                GetItem();
            }

        }
        private void GetItem()
        {
            try
            {
                HttpResponseMessage hrm = client.GetAsync("api/VendorMst").Result;
                if (hrm.IsSuccessStatusCode)
                {
                    VendorMstList = hrm.Content.ReadAsAsync<List<Vendor_Mst_Class>>().Result;
                    vendorGrid.DataSource = VendorMstList.ToList();
                    vendorGrid.DataBind();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private List<Vendor_Mst_Class> GetVendorList()
        {
            try
            {
                HttpResponseMessage hrm = client.GetAsync("api/VendorMst").Result;
                if (hrm.IsSuccessStatusCode)
                {
                    VendorMstList = hrm.Content.ReadAsAsync<List<Vendor_Mst_Class>>().Result;
                }
            }
            catch (Exception ex)
            {
            }
            return VendorMstList;
        }


        protected bool ChkDuplicate()
        {
            bool noduplicate = true;
            for (int i = 0; i < vendorGrid.Rows.Count; i++)
            {
                if (vendorGrid.Rows[i].Cells[2].Text == txtVendor_Name.Text)
                {
                    Response.Write("<script>alert('Exist Data!')</script>");
                    return false;
                }
            }
            return noduplicate;
        }

        protected void btnSaveSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ChkDuplicate())
                {
                    if (btnSaveSave.Text == "Save")
                    {
                        vendorMstEntity.VEN_NAME = txtVendor_Name.Text;
                        vendorMstEntity.VEN_MOB = txtMob.Text;
                        vendorMstEntity.VEN_EMAIL = txtEmail.Text;
                        vendorMstEntity.VEN_ADDRESS = txtAddress.Text;
                        vendorMstEntity.CREATED_BY = Convert.ToInt32(id);
                        vendorMstEntity.CREATED_ON = DateTime.Now;
                        vendorMstEntity.IS_ACTIVE = true;
                        vendorMstEntity.IS_DELETE = false;
                        HttpResponseMessage hrm = client.PostAsJsonAsync("api/VendorMst", vendorMstEntity).Result;
                        if (hrm.IsSuccessStatusCode)
                        {
                            GetItem();
                            Response.Write("<script>alert('Data Save Sucessfully !')</script>");
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        vendorMstEntity.VEN_NAME = txtVendor_Name.Text;
                        vendorMstEntity.VEN_MOB = txtMob.Text;
                        vendorMstEntity.VEN_EMAIL = txtEmail.Text;
                        vendorMstEntity.VEN_ADDRESS = txtAddress.Text;
                        vendorMstEntity.MODIFIED_BY = Convert.ToInt32(id);
                        vendorMstEntity.MODIFIED_ON = DateTime.Now;
                        vendorMstEntity.IS_ACTIVE = true;
                        vendorMstEntity.IS_DELETE = false;
                        HttpResponseMessage hrm = client.PutAsJsonAsync("api/VendorMst/" + hdVenId.Value, vendorMstEntity).Result;
                        if (hrm.IsSuccessStatusCode)
                        {
                            GetItem();
                            Response.Write("<script>alert('Data Update Sucessfully !')</script>");
                        }
                        else
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.ToString() + "');</script>");
            }
        }

        protected void vendorGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int rowIndex = int.Parse(e.CommandArgument.ToString());
            if (e.CommandName == "btnEdit")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "ShowTab2", "ShowTab2();", true);
                hdVenId.Value = vendorGrid.Rows[rowIndex].Cells[1].Text;
                txtVendor_Name.Text = vendorGrid.Rows[rowIndex].Cells[2].Text;
                txtMob.Text = vendorGrid.Rows[rowIndex].Cells[3].Text;
                txtEmail.Text = vendorGrid.Rows[rowIndex].Cells[4].Text;
                txtAddress.Text = vendorGrid.Rows[rowIndex].Cells[5].Text;
                btnSaveSave.Text = "Update";
            }
            if (e.CommandName == "btnDelete")
            {
                vendorMstEntity.CREATED_BY = Convert.ToInt32(id);
                vendorMstEntity.CREATED_ON = DateTime.Now;
                vendorMstEntity.IS_ACTIVE = false;
                vendorMstEntity.IS_DELETE = true;
                HttpResponseMessage hrm = client.PutAsJsonAsync("api/VendorMst" + vendorGrid.Rows[rowIndex].Cells[1].Text, vendorMstEntity).Result;
                if (hrm.IsSuccessStatusCode)
                {
                    GetItem();
                    Response.Write("<script>alert('Data Save Sucessfully !')</script>");
                }
                else
                {

                }
            }
        }

        protected void vendorGrid_PageIndexChanged(object sender, EventArgs e)
        {
            vendorGrid.DataSource = GetVendorList();
            vendorGrid.DataBind();
        }

        protected void vendorGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            vendorGrid.PageIndex = e.NewPageIndex;
            vendorGrid.DataSource = GetVendorList();
            vendorGrid.DataBind();
        }

        protected void btnSaveReset_Click(object sender, EventArgs e)
        {

        }
    }
}
