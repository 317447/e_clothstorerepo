﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Http;
using E_Cloth_Store.Model;
using Newtonsoft.Json;
using System.Configuration;
using System.Text;

namespace E_Cloth_Store.MasterData
{
    public partial class Item_Mst : System.Web.UI.Page
    {
        int id=1;

        List<Item_Mst_Class> itemMstList = null;
        Item_Mst_Class itemMstEntity = new Item_Mst_Class();
        HttpClient client = new HttpClient();
        HttpResponseMessage hrm = null;

        string charachter;
        int count;
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpCookie aCookie2 = Request.Cookies["id"];
            //id = Server.HtmlEncode(aCookie2.Value);

            var appsurl = ConfigurationManager.AppSettings["APIServiceURL"].ToString();
            client.BaseAddress = new Uri(appsurl);
            if (!Page.IsPostBack)
            {
                GetItem();
            }
        }       

        #region GetItemList
        private List<Item_Mst_Class> GetItemList()
        {
            try
            {
                HttpResponseMessage hrm = client.GetAsync("api/ItemMst").Result;
                if (hrm.IsSuccessStatusCode)
                {
                    itemMstList = hrm.Content.ReadAsAsync<List<Item_Mst_Class>>().Result;
                }
            }
            catch (Exception ex)
            {
            }
            return itemMstList;
        }
        #endregion

        #region GetItem
        private void GetItem()
        {
            try
            {
                var query = (from a in GetItemList().ToList() select a).ToList();
                itemGrid.DataSource = query;
                itemGrid.DataBind();
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
        protected bool ChkDuplicate()
        {
            bool noduplicate = true;
            for (int i = 0; i < itemGrid.Rows.Count; i++)
            {
                if (itemGrid.Rows[i].Cells[2].Text == txtItem_Name.Text)
                {
                    Response.Write("<script>alert('Exist Data!')</script>");
                    return false;
                }
            }
            return noduplicate;
        }

        #region Save/Update
        protected void btnItemSave_Click(object sender, EventArgs e)
        {
            try
            {
                //if (ChkDuplicate())
                //{
                    if (btnItemSave.Text == "Save")
                    {
                        itemMstEntity.ITM_NAME = txtItem_Name.Text;
                        itemMstEntity.ITM_DESC = txtItmDescription.Text;
                        itemMstEntity.CREATED_BY = Convert.ToInt32(id);
                        itemMstEntity.CREATED_ON = DateTime.Now;
                        itemMstEntity.IS_ACTIVE = true;
                        itemMstEntity.IS_DELETE = false;
                        HttpResponseMessage hrm = client.PostAsJsonAsync("api/ItemMst", itemMstEntity).Result;
                        if (hrm.IsSuccessStatusCode)
                        {
                            GetItem();
                            Response.Write("<script>alert('Data Save Sucessfully !')</script>");
                        }
                    }
                    else
                    {
                        itemMstEntity.ITM_NAME = txtItem_Name.Text;
                        itemMstEntity.ITM_DESC = txtItmDescription.Text;
                        itemMstEntity.MODIFIED_BY = Convert.ToInt32(id);
                        itemMstEntity.MODIFIED_ON = DateTime.Now;
                        itemMstEntity.IS_ACTIVE = true;
                        itemMstEntity.IS_DELETE = false;
                        HttpResponseMessage hrm = client.PutAsJsonAsync("api/ItemMst/" + hdItemId.Value, itemMstEntity).Result;
                        if (hrm.IsSuccessStatusCode)
                        {
                            GetItem();
                            Response.Write("<script>alert('Data Update Sucessfully !')</script>");
                        }
                    }
               // }
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.ToString() + "');</script>");
            }
        }
        #endregion

        #region Edit/Delete
        protected void itemGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int rowIndex = int.Parse(e.CommandArgument.ToString());
            if (e.CommandName=="btnEdit")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "ShowTab2", "ShowTab2();", true);
                hdItemId.Value = itemGrid.Rows[rowIndex].Cells[1].Text;
                txtItem_Name.Text=itemGrid.Rows[rowIndex].Cells[2].Text;
                txtItmDescription.Text = itemGrid.Rows[rowIndex].Cells[3].Text;
                btnItemSave.Text = "Update";
            }
            if (e.CommandName == "btnDelete")
            {
                itemMstEntity.CREATED_BY = Convert.ToInt32(id);
                itemMstEntity.CREATED_ON = DateTime.Now;
                itemMstEntity.IS_ACTIVE = false;
                itemMstEntity.IS_DELETE = true;
                HttpResponseMessage hrm = client.PutAsJsonAsync("api/ItemMst" + itemGrid.Rows[rowIndex].Cells[1].Text, itemMstEntity).Result;
                if (hrm.IsSuccessStatusCode)
                {
                    GetItem();
                    Response.Write("<script>alert('Data Save Sucessfully !')</script>");
                }
                else
                {

                }
            }
        }
        #endregion

        protected void itemGrid_PageIndexChanged(object sender, EventArgs e)
        {
            itemGrid.DataSource = GetItemList();
            itemGrid.DataBind();
        }
        protected void itemGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            itemGrid.PageIndex = e.NewPageIndex;
            itemGrid.DataSource = GetItemList();
            itemGrid.DataBind();
        }

        protected void btnItemReset_Click(object sender, EventArgs e)
        {

        }
    }
}

