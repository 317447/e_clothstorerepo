﻿using E_Cloth_Store.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace E_Cloth_Store.MasterData
{
    public partial class Unit_Mst : System.Web.UI.Page
    {
        int id = 1;
        List<Unit_Mst_Class> unitMstList = null;
        Unit_Mst_Class unitMstEntity = new Unit_Mst_Class();
        HttpClient client = new HttpClient();
        HttpResponseMessage hrm = null;
        string charachter;
        int count;
        protected void Page_Load(object sender, EventArgs e)
        {
            var appsurl = ConfigurationManager.AppSettings["APIServiceURL"].ToString();
            client.BaseAddress = new Uri(appsurl);
            if (!Page.IsPostBack)
            {
                GetUnit();
            }
        }
        protected bool ChkDuplicate()
        {
            bool noduplicate = true;
            for (int i = 0; i < unitGrid.Rows.Count; i++)
            {
                if (unitGrid.Rows[i].Cells[2].Text == txtUnit_Name.Text)
                {
                    Response.Write("<script>alert('Exist Data!')</script>");
                    return false;
                }
            }
            return noduplicate;
        }

        #region GetUnitList
        private List<Unit_Mst_Class> GetUnitList()
        {
            try
            {
                HttpResponseMessage hrm = client.GetAsync("api/UnitMst").Result;
                if (hrm.IsSuccessStatusCode)
                {
                    unitMstList = hrm.Content.ReadAsAsync<List<Unit_Mst_Class>>().Result;
                }
            }
            catch (Exception ex)
            {
            }
            return unitMstList;
        }
        #endregion

        #region GetUnit
        private void GetUnit()
        {
            try
            {
                var query = (from a in GetUnitList().ToList() select a).ToList();
                unitGrid.DataSource = query;
                unitGrid.DataBind();
            }
            catch (Exception ex)
            {

            }
        }
        #endregion


        protected void unitGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int rowIndex = int.Parse(e.CommandArgument.ToString());
            if (e.CommandName == "btnEdit")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "ShowTab2", "ShowTab2();", true);
                hdUnitId.Value = unitGrid.Rows[rowIndex].Cells[1].Text;
                txtUnit_Name.Text = unitGrid.Rows[rowIndex].Cells[2].Text;
                btnUnitSave.Text = "Update";
            }
            if (e.CommandName == "btnDelete")
            {
                unitMstEntity.CREATED_BY = Convert.ToInt32(id);
                unitMstEntity.CREATED_ON = DateTime.Now;
                unitMstEntity.IS_ACTIVE = false;
                unitMstEntity.IS_DELETE = true;
                HttpResponseMessage hrm = client.PutAsJsonAsync("api/UnitMst" + unitGrid.Rows[rowIndex].Cells[1].Text, unitMstEntity).Result;
                if (hrm.IsSuccessStatusCode)
                {
                    GetUnit();
                    Response.Write("<script>alert('Data Save Sucessfully !')</script>");
                }
                else
                {

                }
            }
        }

        protected void btnUnitSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ChkDuplicate())
                {
                    if (btnUnitSave.Text == "Save")
                    {
                        unitMstEntity.UNT_NAME = txtUnit_Name.Text;
                        unitMstEntity.UNT_DESC = txtDescription.Text;
                        unitMstEntity.CREATED_BY = Convert.ToInt32(id);
                        unitMstEntity.CREATED_ON = DateTime.Now;
                        unitMstEntity.IS_ACTIVE = true;
                        unitMstEntity.IS_DELETE = false;
                        HttpResponseMessage hrm = client.PostAsJsonAsync("api/UnitMst", unitMstEntity).Result;
                        if (hrm.IsSuccessStatusCode)
                        {
                            GetUnit();
                            Response.Write("<script>alert('Data Save Sucessfully !')</script>");
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        unitMstEntity.UNT_NAME = txtUnit_Name.Text;
                        unitMstEntity.UNT_DESC = txtDescription.Text;
                        unitMstEntity.MODIFIED_BY = Convert.ToInt32(id);
                        unitMstEntity.MODIFIED_ON = DateTime.Now;
                        unitMstEntity.IS_ACTIVE = true;
                        unitMstEntity.IS_DELETE = false;
                        HttpResponseMessage hrm = client.PutAsJsonAsync("api/UnitMst/" + hdUnitId.Value, unitMstEntity).Result;
                        if (hrm.IsSuccessStatusCode)
                        {
                            GetUnit();
                            Response.Write("<script>alert('Data Update Sucessfully !')</script>");
                        }
                        else
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.ToString() + "');</script>");
            }

           
        }

        protected void unitGrid_PageIndexChanged(object sender, EventArgs e)
        {
            unitGrid.DataSource = GetUnitList();
            unitGrid.DataBind();
        }

        protected void unitGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            unitGrid.PageIndex = e.NewPageIndex;
            unitGrid.DataSource = GetUnitList();
            unitGrid.DataBind();
        }

        protected void btnUnitReset_Click(object sender, EventArgs e)
        {

        }
    }

}
