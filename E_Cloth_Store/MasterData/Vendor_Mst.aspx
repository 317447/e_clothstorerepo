﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/_ClothStoreMaster.Master" AutoEventWireup="true" CodeBehind="Vendor_Mst.aspx.cs" Inherits="E_Cloth_Store.MasterData.Vendor_Mst" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .hiddencol {
            display: none;
        }
    </style>
    <script src="../ResponsiveTheame/vendors/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var i = document.getElementById("<%=HiddenField1.ClientID %>").value;
            if (i == 1) {
                $("#section-1").show();
            }
            document.getElementById("<%=HiddenField1.ClientID %>").value = 2;

        });
        function ShowTab1() {
            $("#section-1").show();
            $("#section-2").hide();
        }
        function ShowTab2() {
            $("#section-1").hide();
            $("#section-2").show();
        }
        function btnItemSave_onclick() {

        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <asp:HiddenField ID="HiddenField1" runat="server" Value="1" />
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel" style="border-color: #3399FF;">
                <div class="x_title">
                    <h2><i class="fa fa-bars"></i>Vendor Details</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="container">
                        <div id="tabs" class="tabs">
                            <nav>
                                <ul id="tabindex">
                                    <li tabindex="0" class="active"><a href="#section-1" class="icon-shop"><span>View Vendor</span></a></li>
                                    <li><a href="#section-2" class="icon-cup" tabindex="1"><span>Add Vendor</span></a></li>
                                </ul>
                            </nav>
                            <div class="content">
                                <section id="section-1" class="active">
                                    <div class="col-md-12">
                                        <asp:GridView ID="vendorGrid" runat="server" Width="100%" AutoGenerateColumns="false" AllowPaging="true" PageSize="5" OnRowCommand="vendorGrid_RowCommand" OnPageIndexChanged="vendorGrid_PageIndexChanged" OnPageIndexChanging="vendorGrid_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="SL No" ItemStyle-Width="100">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="VEN_ID" DataField="VEN_ID" HeaderStyle-CssClass="hiddencol" ItemStyle-CssClass="hiddencol" />
                                                <asp:BoundField HeaderText="Vendor Name" DataField="VEN_NAME" />
                                                <asp:BoundField HeaderText="Vendor Mobile" DataField="VEN_MOB" />
                                                <asp:BoundField HeaderText="Vendor Email" DataField="VEN_EMAIL" />
                                                <asp:BoundField HeaderText="Vendor Address" DataField="VEN_ADDRESS" />
                                                <asp:ButtonField CommandName="btnEdit" ButtonType="Image" ImageUrl="~/Image/images/ico/icon_edit.png" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center" />
                                                <%--<asp:ButtonField CommandName="btnDelete" ButtonType="Image" ImageUrl="~/Image/images/ico/ico_delete.png" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center" />--%>
                                            </Columns>
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />
                                            <FooterStyle BackColor="White" ForeColor="#000066" />
                                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" CssClass="paging" />
                                            <RowStyle ForeColor="#000066" />
                                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                        </asp:GridView>
                                    </div>
                                </section>
                                <section id="section-2" class="active">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <asp:HiddenField ID="hdVenId" runat="server" />
                                            <div class="from-group">
                                                <div class="col-md-3">
                                                    <asp:Label ID="Label1" runat="server">Vendor Name :</asp:Label>
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:TextBox ID="txtVendor_Name" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="clearfix" style="margin-bottom: 2%"></div>
                                            <div class="from-group">
                                                <div class="col-md-3">
                                                    <asp:Label ID="lblMob" runat="server">Mobile :</asp:Label>
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:TextBox ID="txtMob" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="clearfix" style="margin-bottom: 2%"></div>
                                            <div class="from-group">
                                                <div class="col-md-3">
                                                    <asp:Label ID="lblEmail" runat="server">Email :</asp:Label>
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="clearfix" style="margin-bottom: 2%"></div>
                                            <div class="from-group">
                                                <div class="col-md-3">
                                                    <asp:Label ID="lblAddress" runat="server">Address :</asp:Label>
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:TextBox ID="txtAddress" runat="server" class="form-control" Height="100px" TextMode="MultiLine"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix" style="margin-bottom: 2%"></div>
                                    <div class="col-md-12">
                                        <div class="from-group">
                                            <div class="col-md-7" align="center">
                                                <asp:Button ID="btnSaveReset" Text="Reset" runat="server" CssClass="btn btn-warning" Width="12%" OnClick="btnSaveReset_Click"/>
                                                <asp:Button ID="btnSaveSave" Text="Save" runat="server" CssClass="btn btn-success" Width="12%" OnClick="btnSaveSave_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
