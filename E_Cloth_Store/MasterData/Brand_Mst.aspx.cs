﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Http;
using E_Cloth_Store.Model;
using Newtonsoft.Json;
using System.Configuration;
using E_Cloth_DataAccess;
namespace E_Cloth_Store.MasterData
{
    public partial class Brand_Mst : System.Web.UI.Page
    {
        E_Cloth_StoreEntities objEntity = new E_Cloth_StoreEntities();
        int id = 1;
        List<Brand_Mst_Class> brandMstList = null;
        List<Item_Mst_Class> itemMstList = null;
        Brand_Mst_Class brandMstEntity = new Brand_Mst_Class();
        HttpClient client = new HttpClient();
        HttpResponseMessage hrm = null;
        string charachter;
        int count;
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpCookie aCookie2 = Request.Cookies["id"];
            //id = Server.HtmlEncode(aCookie2.Value);

            var appsurl = ConfigurationManager.AppSettings["APIServiceURL"].ToString();
            client.BaseAddress = new Uri(appsurl);
            if (!Page.IsPostBack)
            {
                BindItem();
                GetBrand();
            }
        }

        #region GetBrandList
        private List<Brand_Mst_Class> GetBrandList()
        {
            try
            {
                HttpResponseMessage hrm = client.GetAsync("api/BrandMst").Result;
                if (hrm.IsSuccessStatusCode)
                {
                    brandMstList = hrm.Content.ReadAsAsync<List<Brand_Mst_Class>>().Result;
                }
            }
            catch (Exception ex)
            {
            }
            return brandMstList;
        }
        #endregion

        #region GetItemList
        private List<Item_Mst_Class> GetItemList()
        {
            try
            {
                HttpResponseMessage hrm = client.GetAsync("api/ItemMst").Result;
                if (hrm.IsSuccessStatusCode)
                {
                    itemMstList = hrm.Content.ReadAsAsync<List<Item_Mst_Class>>().Result;
                }
            }
            catch (Exception ex)
            {
            }
            return itemMstList;
        }
        #endregion

        #region BindItem
        private void BindItem()
        {
            try
            {
                var query = (from a in GetItemList().ToList() select a).ToList();
                ddlItem.DataSource = query;
                ddlItem.DataValueField = "ITM_ID";
                ddlItem.DataTextField = "ITM_NAME";
                ddlItem.DataBind();
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        #region GetBrand
        private void GetBrand()
        {
            try
            {
                var query = (from a in GetBrandList().ToList()
                             join b in GetItemList().ToList() on a.ITM_ID equals b.ITM_ID
                             select new
                             {
                                 a.BR_ID,
                                 a.BR_NAME,
                                 a.BR_DESC,
                                 b.ITM_ID,
                                 b.ITM_NAME,
                             }).ToList();
                brandGrid.DataSource = query;
                brandGrid.DataBind();
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        protected bool ChkDuplicate()
        {
            bool noduplicate = true;
            for (int i = 0; i < brandGrid.Rows.Count; i++)
            {
                if (brandGrid.Rows[i].Cells[2].Text == txtBrand_Name.Text)
                {
                    Response.Write("<script>alert('Exist Data!')</script>");
                    return false;
                }
            }
            return noduplicate;
        }
        protected void btnBrandSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ChkDuplicate())
                {
                    if (btnBrandSave.Text == "Save")
                    {
                        brandMstEntity.ITM_ID = Convert.ToInt32(ddlItem.SelectedValue);
                        brandMstEntity.BR_NAME = txtBrand_Name.Text;
                        brandMstEntity.BR_DESC = txtDescription.Text;
                        brandMstEntity.CREATED_BY = Convert.ToInt32(id);
                        brandMstEntity.CREATED_ON = DateTime.Now;
                        brandMstEntity.IS_ACTIVE = true;
                        brandMstEntity.IS_DELETE = false;
                        HttpResponseMessage hrm = client.PostAsJsonAsync("api/BrandMst", brandMstEntity).Result;
                        if (hrm.IsSuccessStatusCode)
                        {
                            GetBrand();
                            Response.Write("<script>alert('Data Save Sucessfully !')</script>");
                        }
                    }
                    else
                    {
                        brandMstEntity.ITM_ID = Convert.ToInt32(ddlItem.SelectedValue);
                        brandMstEntity.BR_NAME = txtBrand_Name.Text;
                        brandMstEntity.BR_DESC = txtDescription.Text;
                        brandMstEntity.CREATED_BY = Convert.ToInt32(id);
                        brandMstEntity.CREATED_ON = DateTime.Now;
                        brandMstEntity.IS_ACTIVE = true;
                        brandMstEntity.IS_DELETE = false;
                        HttpResponseMessage hrm = client.PutAsJsonAsync("api/BrandMst/" + hdBrandId.Value, brandMstEntity).Result;
                        if (hrm.IsSuccessStatusCode)
                        {
                            GetBrand();
                            Response.Write("<script>alert('Data Update Sucessfully !')</script>");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.ToString() + "');</script>");
            }
        }

        protected void brandGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            int rowIndex = int.Parse(e.CommandArgument.ToString());
            if (e.CommandName == "btnEdit")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "ShowTab2", "ShowTab2();", true);
                hdBrandId.Value = brandGrid.Rows[rowIndex].Cells[1].Text;
                ddlItem.SelectedValue = brandGrid.Rows[rowIndex].Cells[2].Text;
                ddlItem.SelectedItem.Text = brandGrid.Rows[rowIndex].Cells[3].Text;
                txtBrand_Name.Text = brandGrid.Rows[rowIndex].Cells[4].Text;
                txtDescription.Text = brandGrid.Rows[rowIndex].Cells[5].Text;
                btnBrandSave.Text = "Update";
            }
            if (e.CommandName == "btnDelete")
            {
                brandMstEntity.CREATED_BY = Convert.ToInt32(id);
                brandMstEntity.CREATED_ON = DateTime.Now;
                brandMstEntity.IS_ACTIVE = false;
                brandMstEntity.IS_DELETE = true;
                HttpResponseMessage hrm = client.PutAsJsonAsync("api/BrandMst/" + brandGrid.Rows[rowIndex].Cells[1].Text, brandMstEntity).Result;
                if (hrm.IsSuccessStatusCode)
                {
                    GetBrand();
                    Response.Write("<script>alert('Data Delete Sucessfully !')</script>");
                }
            }
        }

        protected void brandGrid_PageIndexChanged(object sender, EventArgs e)
        {
            brandGrid.DataSource = GetBrandList();
            brandGrid.DataBind();
        }

        protected void brandGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            brandGrid.PageIndex = e.NewPageIndex;
            brandGrid.DataSource = GetBrandList();
            brandGrid.DataBind();
        }

        protected void btnBrandReset_Click(object sender, EventArgs e)
        {

        }
    }
}



