﻿using E_Cloth_Store.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace E_Cloth_Store.MasterData
{
    public partial class Tax_Mst : System.Web.UI.Page
    {
        int id = 1;
        List<Tax_Mst_Class> taxMstList = null;
        Tax_Mst_Class taxMstEntity = new Tax_Mst_Class();
        HttpClient client = new HttpClient();
        HttpResponseMessage hrm = null;
        string charachter;
        int count;
        protected void Page_Load(object sender, EventArgs e)
        {
            var appsurl = ConfigurationManager.AppSettings["APIServiceURL"].ToString();
            client.BaseAddress = new Uri(appsurl);
            if (!IsPostBack)
            {
                GetTax();
            }
        }
        private void GetTax()
        {
            try
            {
                HttpResponseMessage hrm = client.GetAsync("api/TaxMst").Result;
                if (hrm.IsSuccessStatusCode)
                {
                    taxMstList = hrm.Content.ReadAsAsync<List<Tax_Mst_Class>>().Result;
                    TaxGrid.DataSource = taxMstList.ToList();
                    TaxGrid.DataBind();
                }
            }
            catch (Exception ex)
            {

            }
        }

        private List<Tax_Mst_Class> GetTaxList()
        {
            try
            {
                HttpResponseMessage hrm = client.GetAsync("api/TaxMst").Result;
                if (hrm.IsSuccessStatusCode)
                {
                    taxMstList = hrm.Content.ReadAsAsync<List<Tax_Mst_Class>>().Result;
                }
            }
            catch (Exception ex)
            {
            }
            return taxMstList;
        }
        protected bool ChkDuplicate()
        {
            bool noduplicate = true;
            for (int i = 0; i < TaxGrid.Rows.Count; i++)
            {
                if (TaxGrid.Rows[i].Cells[2].Text == txtTax_Description.Text)
                {
                    Response.Write("<script>alert('Exist Data!')</script>");
                    return false;
                }
            }
            return noduplicate;
        }



        protected void btnTaxSave_Click(object sender, EventArgs e)
        {

            try
            {
                if (ChkDuplicate())
                {
                    if (btnTaxSave.Text == "Save")
                    {
                        taxMstEntity.TAX_DESC = txtTax_Description.Text;
                        taxMstEntity.TAX_RATE = Convert.ToDecimal(txttax_Rate.Text);
                        taxMstEntity.CREATED_BY = Convert.ToInt32(id);
                        taxMstEntity.CREATED_ON = DateTime.Now;
                        taxMstEntity.IS_ACTIVE = true;
                        taxMstEntity.IS_DELETE = false;
                        HttpResponseMessage hrm = client.PostAsJsonAsync("api/TaxMst", taxMstEntity).Result;
                        if (hrm.IsSuccessStatusCode)
                        {
                            GetTax();
                            Response.Write("<script>alert('Data Save Sucessfully !')</script>");
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        taxMstEntity.TAX_DESC = txtTax_Description.Text;
                        taxMstEntity.TAX_RATE = Convert.ToDecimal(txttax_Rate.Text);
                        taxMstEntity.MODIFIED_BY = Convert.ToInt32(id);
                        taxMstEntity.MODIFIED_ON = DateTime.Now;
                        taxMstEntity.IS_ACTIVE = true;
                        taxMstEntity.IS_DELETE = false;
                        HttpResponseMessage hrm = client.PutAsJsonAsync("api/TaxMst/" + hdTaxId.Value, taxMstEntity).Result;
                        if (hrm.IsSuccessStatusCode)
                        {
                            GetTax();
                            btnTaxSave.Text = "Save";
                            Response.Write("<script>alert('Data Update Sucessfully !')</script>");
                        }
                        else
                        {

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script>alert('" + ex.ToString() + "');</script>");
            }
        }

        protected void TaxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            int rowIndex = int.Parse(e.CommandArgument.ToString());
            if (e.CommandName == "btnEdit")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "ShowTab2", "ShowTab2();", true);
                hdTaxId.Value = TaxGrid.Rows[rowIndex].Cells[1].Text;
                txttax_Rate.Text = TaxGrid.Rows[rowIndex].Cells[2].Text;
                txtTax_Description.Text = TaxGrid.Rows[rowIndex].Cells[3].Text;
                btnTaxSave.Text = "Update";
            }
            if (e.CommandName == "btnDelete")
            {
                taxMstEntity.CREATED_BY = Convert.ToInt32(id);
                taxMstEntity.CREATED_ON = DateTime.Now;
                taxMstEntity.IS_ACTIVE = false;
                taxMstEntity.IS_DELETE = true;
                HttpResponseMessage hrm = client.PutAsJsonAsync("api/TaxMst" + TaxGrid.Rows[rowIndex].Cells[1].Text, taxMstEntity).Result;
                if (hrm.IsSuccessStatusCode)
                {
                    GetTax();
                    Response.Write("<script>alert('Data Save Sucessfully !')</script>");
                }
                else
                {

                }
            }
        }

        protected void TaxGrid_PageIndexChanged(object sender, EventArgs e)
        {
            TaxGrid.DataSource = GetTaxList();
            TaxGrid.DataBind();
        }

        protected void TaxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            TaxGrid.PageIndex = e.NewPageIndex;
            TaxGrid.DataSource = GetTaxList();
            TaxGrid.DataBind();
        }

        protected void btnTaxReset_Click(object sender, EventArgs e)
        {

        }
    }
}

