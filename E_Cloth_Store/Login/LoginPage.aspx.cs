﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using E_Cloth_DataAccess;
using E_Cloth_Business;

namespace E_Cloth_Store.Login
{
    public partial class LoginPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                var regd = _CShopManagement.GetInstance.GetRegdDet().ToList();

                foreach (var item in regd)
                {
                    if (item.REG_EMAIL == txtEmail.Text && item.REG_PASSWORD == txtPassword.Text)
                    {
                        if (item.REG_ACTIVITY == "Admin")
                        {
                            if (Request.Browser.Cookies)
                            {
                                HttpCookie cookie = new HttpCookie("role");
                                cookie.Value = "Admin";
                                cookie.Expires = DateTime.Now.AddDays(1);
                                Response.Cookies.Add(cookie);
                                HttpCookie cookie1 = new HttpCookie("email");
                                cookie1.Value = txtEmail.Text;
                                cookie1.Expires = DateTime.Now.AddDays(1);
                                Response.Cookies.Add(cookie1);
                                HttpCookie cookie2 = new HttpCookie("id");
                                cookie2.Value = Convert.ToString(item.REG_ID);
                                cookie2.Expires = DateTime.Now.AddDays(1);
                                Response.Cookies.Add(cookie2);
                                Response.Redirect("../Admin/AdminDashBoard.aspx");
                            }
                            else
                            {

                            }
                        }
                        else
                        {
                            HttpCookie cookie = new HttpCookie("role");
                            cookie.Value = "User";
                            cookie.Value = txtEmail.Text;
                            cookie.Expires = DateTime.Now.AddDays(1);
                            Response.Cookies.Add(cookie);
                            HttpCookie cookie1 = new HttpCookie("email");
                            cookie1.Value = txtEmail.Text;
                            cookie1.Expires = DateTime.Now.AddDays(1);
                            Response.Cookies.Add(cookie1);
                            Response.Redirect("../User/UserDashBoard.aspx");
                        }
                    }
                    else
                    {

                    }
                }
            }
            catch (Exception)
            {
            }
        }
    }
}