﻿<%@ Page EnableEventValidation="false" Language="C#" AutoEventWireup="true" CodeBehind="SignUpPage.aspx.cs" Inherits="E_Cloth_Store.Login.SignUpPage"  %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>E-Shop | Home :: |</title>
    <!--css-->
    <link href="../Style/css/css/coreSlider.css" rel="stylesheet" />
    <link href="../Style/css/css/bootstrap.css" rel="stylesheet" media="all" />
    <link href="../Style/css/css/style.css" rel="stylesheet" media="all" />
    <link href="../Style/css/css/font-awesome.css" rel="stylesheet" />
    <link href="../Style/css/css/font-awesome.css" rel="stylesheet" />
    
    <!--css-->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="New Shop Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

    <script src="../Script/js/js/jquery.min.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Cagliostro' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800italic,800,700italic,700,600italic,600,400italic,300italic,300' rel='stylesheet' type='text/css' />
    <!--search jQuery-->
    <script src="../Script/js/js/main.js"></script>
    <!--search jQuery-->
    <script src="../Script/js/js/responsiveslides.min.js"></script>
    <script src="../Script/js/js/coreSlider.js"></script>

    <script>
        $(function () {
            $("#slider").responsiveSlides({
                auto: true,
                nav: true,
                speed: 500,
                namespace: "callbacks",
                pager: true,
            });
        });
    </script>
    <!--mycart-->
    <script src="../Script/js/js/bootstrap-3.1.1.min.js"></script>
    <!-- cart -->
    <script src="../Script/js/js/simpleCart.min.js"></script>
    <!-- cart -->
    <!--start-rate-->
    <script src="../Script/js/js/jstarbox.js"></script>
    <link href="../Style/css/css/jstarbox.css" rel="stylesheet" type="text/css" media="screen" charset="utf-8" />
    <script type="text/javascript">
        jQuery(function () {
            jQuery('.starbox').each(function () {
                var starbox = jQuery(this);
                starbox.starbox({
                    average: starbox.attr('data-start-value'),
                    changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
                    ghosting: starbox.hasClass('ghosting'),
                    autoUpdateAverage: starbox.hasClass('autoupdate'),
                    buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
                    stars: starbox.attr('data-star-count') || 5
                }).bind('starbox-value-changed', function (event, value) {
                    if (starbox.hasClass('random')) {
                        var val = Math.random();
                        starbox.next().text(' ' + val);
                        return val;
                    }
                })
            });
        });
    </script>
    <!--//End-rate-->
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <!--header-->
		<div class="header">
        <div class="header-top">
            <div class="container">
                <div class="top-left">
                    <a href="#">Help  <i class="glyphicon glyphicon-phone" aria-hidden="true"></i>+91-9778998826</a>
                </div>
                <div class="top-right">
                    <ul>
                        <li><a href="LoginPage.aspx">Login</a></li>
                        <li><a href="SignUpPage.aspx">Create Account </a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="heder-bottom">
            <div class="container">
                <div class="logo-nav">
                    <div class="logo-nav-left">
                        <h1><a href="#">Cloth Shop <span>Shopping anywhere</span></a></h1>
                    </div>
                    <div class="logo-nav-left1">
                        <nav class="navbar navbar-default">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header nav_2">
                                <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                                <ul class="nav navbar-nav">
                                    <li class="active"><a href="../websites.aspx" class="act">Home</a></li>
                                    <li><a href="MailUs.aspx">Mail Us</a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                    <div class="logo-nav-right">
                        <ul class="cd-header-buttons">
                            <li><a class="cd-search-trigger" href="#cd-search"><span></span></a></li>
                        </ul>
                        <!-- cd-header-buttons -->
                        <div id="cd-search" class="cd-search">
                            <form action="#" method="post">
                                <input name="Search" type="search" placeholder="Search..." />
                            </form>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
		<!--header-->
		<!--banner-->
		<div class="banner1">
			<div class="container">
				<h3><a href="../websites.aspx">Home</a> / <span>Registration</span></h3>
			</div>
		</div>
	    <!--banner-->

	    <!--content-->
		<div class="content">
				<!--login-->
			<div class="login">
		    <div class="main-agileits">
				<div class="form-w3agile form1">
					<h3>Register</h3>                    
						<div class="key">
							<i class="fa fa-user" aria-hidden="true"></i>
							<asp:TextBox ID="txtUserName" runat="server" value="Username" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Username';}" required=""></asp:TextBox>
							<div class="clearfix"></div>
						</div>
						<div class="key">
							<i class="fa fa-envelope" aria-hidden="true"></i>
							<asp:TextBox ID="txtEmail" runat="server" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required=""/>
							<div class="clearfix"></div>
						</div>
                        <div class="key">
							<i class="fa fa-phone" aria-hidden="true"></i>
							<asp:TextBox ID="txtPhone" runat="server" value="Phone" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Phone';}" required=""/>
							<div class="clearfix"></div>
						</div>
                        <div class="key">
							<i class="fa fa-book" aria-hidden="true"></i>
							<asp:TextBox ID="txtAddress" runat="server" value="Address" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Address';}" required=""/>
							<div class="clearfix"></div>
						</div>
						<div class="key">
							<i class="fa fa-lock" aria-hidden="true"></i>
							<asp:TextBox ID="txtPassword" runat="server"  type="password" value="Password"  onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required=""/>
							<div class="clearfix"></div>
						</div>
						<div class="key">
							<i class="fa fa-lock" aria-hidden="true"></i>
							<asp:TextBox ID="txtConPassword" runat="server"  type="password" value="Confirm Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Confirm Password';}" required=""/>
							<div class="clearfix"></div>
						</div>
						<asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
				</div>				
			</div>
		</div>
				<!--login-->
		</div>
		<!--content-->
		<!---footer--->
		<div class="footer-w3l">
						<div class="container">
							<div class="footer-grids">
								 <div class="col-md-3 footer-grid">
                                    <h4>About </h4>
                                    <p>ClothShop is the fun, friendly spot for style and decor that’s as expressive and unique as you are!</p>
                                    <div class="social-icon">
                                        <a href="http://www.facebook.com"><i class="icon"></i></a>
                                        <a href="http://www.twitter.com"><i class="icon1"></i></a>
                                        <a href="http://www.plus.google.com"><i class="icon2"></i></a>
                                        <a href="http://www.linkedin.com"><i class="icon3"></i></a>
                                    </div>
                                </div>
								<div class="col-md-3 footer-grid">
									<h4>My Account</h4>
									<ul>
										<li><a href="LoginPage.aspx">Login</a></li>
                                        <li><a href="SignUpPage.aspx">Create Account </a></li>
									</ul>
								</div>
								<div class="col-md-3 footer-grid">
									<h4>Information</h4>
									<ul>
										<li><a href="../websites.aspx">Home</a></li>
										<li><a href="MailUs.aspx">Mail Us</a></li>
									</ul>
								</div>
								<div class="col-md-3 footer-grid foot">
									<h4>Contacts</h4>
									<ul>
										<li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i><a href="#">Bhubaneswar, Odisha, India</a></li>
                                        <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i><a href="#">+91-8093889996</a></li>
                                        <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i><a href="mailto:jyoti.panda92@ymail.com">jyoti.panda92@ymail.com</a></li>										
									</ul>
								</div>
							<div class="clearfix"> </div>
							</div>
							
						</div>
					</div>
		<!---footer--->
		<!--copy-->
		<div class="copy-section">
        <div class="container">
            <div class="copy-left">
                <p>&copy; 2016-2017 Cloth Shop . All rights reserved | Design by <a href="http://facebook.com">Jyoti Prakash Panda</a></p>
            </div>
            <div class="copy-right">
                <img src="../Image/images/images/card.png" alt="" />
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
		<!--copy-->
    </div>
    </form>
</body>
</html>
