﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using E_Cloth_DataAccess;
using E_Cloth_Business;

namespace E_Cloth_Store.Login
{
    public partial class SignUpPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            TBL_REGD_MST regdMst = new TBL_REGD_MST();
            regdMst.REG_NAME = txtUserName.Text;
            regdMst.REG_EMAIL = txtEmail.Text;
            regdMst.REG_PHONE = txtPhone.Text;
            regdMst.REG_PASSWORD = txtPassword.Text;
            regdMst.REG_ACTIVITY = "User";
            regdMst.REG_ADDRESS = txtAddress.Text;
            regdMst.CREATED_ON = System.DateTime.Now;
            regdMst.IS_ACTIVE = false;
            regdMst.IS_DELETE = false;
            _CShopManagement.GetInstance.SaveRegdDet(regdMst);
        }

    }
}