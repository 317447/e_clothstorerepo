﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using E_Cloth_Business;
using System.Text;
using Newtonsoft.Json.Linq;
using E_Cloth_DataAccess;
using System.Data.Entity;
using WebApiService.Models;
namespace WebApiService.Controllers
{
    public class BrandMstController : ApiController
    {
        // GET: api/BrandMst
        public HttpResponseMessage Get()
        {
            try
            {
                var objGet = _CShopManagement.GetInstance.BrndViewBindBA();
                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(JArray.FromObject(objGet).ToString(), Encoding.UTF8, "application/Json")
                };
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);

            }
        }
        // GET: api/BrandMst/5
        public string Get(int id)
        {
            return "value";
        }
        // POST: api/BrandMst
        public HttpResponseMessage Post([FromBody]Brand_Mst_Class clsBrand)
        {
            try
            {
                TBL_BRAND_MST objBrand = new TBL_BRAND_MST();
                objBrand.BR_NAME = clsBrand.BR_NAME;
                objBrand.BR_DESC = clsBrand.BR_DESC;
                objBrand.ITM_ID = clsBrand.ITM_ID;
                objBrand.CREATED_ON = clsBrand.CREATED_ON;
                objBrand.CREATED_BY = clsBrand.CREATED_BY;
                objBrand.IS_ACTIVE = clsBrand.IS_ACTIVE;
                objBrand.IS_DELETE = clsBrand.IS_DELETE;
                _CShopManagement.GetInstance.InsertBrand(objBrand);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }
        // PUT: api/BrandMst/5
        public HttpResponseMessage Put(int id, [FromBody]Brand_Mst_Class clsBrand)
        {
            try
            {
                TBL_BRAND_MST objBrand = new TBL_BRAND_MST();
                objBrand.BR_NAME = clsBrand.BR_NAME;
                objBrand.BR_DESC = clsBrand.BR_DESC;
                objBrand.ITM_ID = clsBrand.ITM_ID;
                objBrand.IS_ACTIVE = true;
                objBrand.IS_DELETE = false;
                _CShopManagement.GetInstance.UpdateBrand(objBrand, id);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }
        // DELETE: api/BrandMst/5
        public HttpResponseMessage Delete(int id)
        {
            try
            {
              TBL_BRAND_MST   objBrand = new TBL_BRAND_MST();
              objBrand.IS_ACTIVE = false;
              objBrand.IS_DELETE = true;
              _CShopManagement.GetInstance.DeleteBrand(objBrand, id);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }
    }
}
