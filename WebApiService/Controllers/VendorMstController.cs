﻿using E_Cloth_Business;
using E_Cloth_DataAccess;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using WebApiService.Models;

namespace WebApiService.Controllers
{
    public class VendorMstController : ApiController
    {
        // GET: api/VendorMst
        public HttpResponseMessage Get()
        {
            try
            {
                var objGet = _CShopManagement.GetInstance.VendorViewBindBA();
                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(JArray.FromObject(objGet).ToString(), Encoding.UTF8, "application/Json")
                };
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);

            }
        }
        // GET: api/VendorMst/5
        public string Get(int id)
        {
            return "value";
        }
        // POST: api/VendorMst
        public HttpResponseMessage Post([FromBody]Vendor_Mst_Class clsVendor)
        {
            try
            {
                TBL_VENDOR_MST objVendor = new TBL_VENDOR_MST();
                objVendor.VEN_ID = clsVendor.VEN_ID;
               // objVendor.VEN_CODE = clsVendor.VEN_CODE;
                objVendor.VEN_NAME = clsVendor.VEN_NAME;
                objVendor.VEN_MOB = clsVendor.VEN_MOB;
                objVendor.VEN_EMAIL = clsVendor.VEN_EMAIL;
                objVendor.VEN_ADDRESS = clsVendor.VEN_ADDRESS;
                objVendor.MODIFIED_ON = clsVendor.MODIFIED_ON;
                objVendor.MODIFIED_BY = clsVendor.MODIFIED_BY;
                objVendor.IS_ACTIVE = clsVendor.IS_ACTIVE;
                objVendor.IS_DELETE = clsVendor.IS_DELETE;
                _CShopManagement.GetInstance.InsertVendor(objVendor);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }
      
        // PUT: api/VendorMst/5
        public HttpResponseMessage Put(int id, [FromBody]Vendor_Mst_Class clsVendor)
        {
            try
            {
                if (clsVendor.IS_ACTIVE == true)
                {
                    TBL_VENDOR_MST objVendor = new TBL_VENDOR_MST();
                    objVendor.VEN_NAME = clsVendor.VEN_NAME;
                    objVendor.VEN_MOB = clsVendor.VEN_MOB;
                    objVendor.VEN_EMAIL = clsVendor.VEN_EMAIL;
                    objVendor.VEN_ADDRESS = clsVendor.VEN_ADDRESS;
                    objVendor.MODIFIED_BY = clsVendor.CREATED_BY;
                    objVendor.MODIFIED_ON = clsVendor.CREATED_ON;
                    objVendor.IS_ACTIVE = clsVendor.IS_ACTIVE;
                    objVendor.IS_DELETE = clsVendor.IS_DELETE;
                    _CShopManagement.GetInstance.UpdateVendor(objVendor, id);
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                else
                {
                    TBL_VENDOR_MST objVendor = new TBL_VENDOR_MST();
                    objVendor.VEN_NAME = clsVendor.VEN_NAME;
                    objVendor.VEN_MOB = clsVendor.VEN_MOB;
                    objVendor.VEN_EMAIL = clsVendor.VEN_EMAIL;
                    objVendor.VEN_ADDRESS = clsVendor.VEN_ADDRESS;
                    objVendor.MODIFIED_BY = clsVendor.CREATED_BY;
                    objVendor.MODIFIED_ON = clsVendor.CREATED_ON;
                    objVendor.IS_ACTIVE = clsVendor.IS_ACTIVE;
                    objVendor.IS_DELETE = clsVendor.IS_DELETE;
                    _CShopManagement.GetInstance.DeleteVendor(objVendor, id);
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }
        // DELETE: api/VendorMst/5
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                TBL_VENDOR_MST objVendor = new TBL_VENDOR_MST();
                objVendor.IS_ACTIVE = false;
                objVendor.IS_DELETE = true;
                _CShopManagement.GetInstance.DeleteVendor(objVendor, id);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }
    }
}
