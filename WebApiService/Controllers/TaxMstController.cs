﻿using E_Cloth_Business;
using E_Cloth_DataAccess;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using WebApiService.Models;

namespace WebApiService.Controllers
{
    public class TaxMstController : ApiController
    {
        // GET: api/TaxMst
        public HttpResponseMessage Get()
        {
            try
            {
                var objGet = _CShopManagement.GetInstance.TaxViewBindBA();
                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(JArray.FromObject(objGet).ToString(), Encoding.UTF8, "application/Json")
                };
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);

            }
        }
        // GET: api/TaxMst/5
        public string Get(int id)
        {
            return "value";
        }
        // POST: api/TaxMst
        public HttpResponseMessage Post([FromBody]Tax_Mst_Class clsTax)
        {
            try
            {
                TBL_TAX_MST objTax = new TBL_TAX_MST();
                objTax.TAX_ID = clsTax.TAX_ID;
                objTax.TAX_DESC = clsTax.TAX_DESC;
                objTax.TAX_RATE = clsTax.TAX_RATE;
                objTax.CREATED_ON = clsTax.CREATED_ON;
                objTax.CREATED_BY = clsTax.CREATED_BY;
                objTax.IS_ACTIVE = clsTax.IS_ACTIVE;
                objTax.IS_DELETE = clsTax.IS_DELETE;
                _CShopManagement.GetInstance.InsertTax(objTax);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }

        // PUT: api/TaxMst/5
        public HttpResponseMessage Put(int id, [FromBody]Tax_Mst_Class clsTax)
        {
            try
            {
                if (clsTax.IS_ACTIVE == true)
                {
                    TBL_TAX_MST objTax = new TBL_TAX_MST();
                    objTax.TAX_DESC = clsTax.TAX_DESC;
                    objTax.TAX_RATE = clsTax.TAX_RATE;
                    objTax.MODIFIED_BY = clsTax.MODIFIED_BY;
                    objTax.MODIFIED_ON = clsTax.MODIFIED_ON;
                    objTax.IS_ACTIVE = clsTax.IS_ACTIVE;
                    objTax.IS_DELETE = clsTax.IS_DELETE;
                    _CShopManagement.GetInstance.UpdateTax(objTax, id);
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                else
                {
                    TBL_TAX_MST objTax = new TBL_TAX_MST();
                    objTax.MODIFIED_BY = clsTax.CREATED_BY;
                    objTax.MODIFIED_ON = clsTax.CREATED_ON;
                    objTax.IS_ACTIVE = clsTax.IS_ACTIVE;
                    objTax.IS_DELETE = clsTax.IS_DELETE;
                    _CShopManagement.GetInstance.DeleteTax(objTax, id);
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }
        // DELETE: api/TaxMst/5
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                TBL_TAX_MST objTax = new TBL_TAX_MST();
                objTax.IS_ACTIVE = false;
                objTax.IS_DELETE = true;
                _CShopManagement.GetInstance.DeleteTax(objTax, id);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }
    }
}
