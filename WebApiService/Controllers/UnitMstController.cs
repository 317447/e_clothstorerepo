﻿using E_Cloth_Business;
using E_Cloth_DataAccess;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using WebApiService.Models;

namespace WebApiService.Controllers
{
    public class UnitMstController : ApiController
    {
        // GET: api/UnitMst
        public HttpResponseMessage Get()
        {
            try
            {
                var objGet = _CShopManagement.GetInstance.UnitViewBindBA();
                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(JArray.FromObject(objGet).ToString(), Encoding.UTF8, "application/Json")
                };
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);

            }
        }
        // GET: api/UnitMst/5
        public string Get(int id)
        {
            return "value";
        }
        // POST: api/UnitMst
        public HttpResponseMessage Post([FromBody]Unit_Mst_Class clsUnit)
        {
            try
            {
                TBL_UNIT_MST objUnit = new TBL_UNIT_MST();
                objUnit.UNT_NAME = clsUnit.UNT_NAME;
                objUnit.UNT_DESC = clsUnit.UNT_DESC;
                objUnit.CREATED_ON = clsUnit.CREATED_ON;
                objUnit.CREATED_BY = clsUnit.CREATED_BY;
                objUnit.IS_ACTIVE = clsUnit.IS_ACTIVE;
                objUnit.IS_DELETE = clsUnit.IS_DELETE;
                _CShopManagement.GetInstance.InsertUnit(objUnit);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }
        // PUT: api/UnitMst/5
        public HttpResponseMessage Put(int id, [FromBody]Unit_Mst_Class clsUnit)
        {
            try
            {
                if (clsUnit.IS_ACTIVE==true)
                {
                    TBL_UNIT_MST objunit = new TBL_UNIT_MST();
                    objunit.UNT_NAME = clsUnit.UNT_NAME;
                    objunit.UNT_DESC = clsUnit.UNT_DESC;
                    objunit.MODIFIED_BY = clsUnit.MODIFIED_BY;
                    objunit.MODIFIED_ON = clsUnit.MODIFIED_ON;
                    objunit.IS_ACTIVE = true;
                    objunit.IS_DELETE = false;
                    _CShopManagement.GetInstance.UpdateUnit(objunit, id);
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                else
                {
                    TBL_UNIT_MST objunit = new TBL_UNIT_MST();
                    objunit.UNT_NAME = clsUnit.UNT_NAME;
                    objunit.IS_ACTIVE = true;
                    objunit.IS_DELETE = false;
                    _CShopManagement.GetInstance.DeleteUnit(objunit, id);
                    return new HttpResponseMessage(HttpStatusCode.OK);
                    
                }
               
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }
        // DELETE: api/BrandMst/5
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }
    }
}
