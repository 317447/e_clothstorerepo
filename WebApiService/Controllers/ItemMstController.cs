﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using E_Cloth_Business;
using System.Text;
using Newtonsoft.Json.Linq;
using E_Cloth_DataAccess;
using System.Data.Entity;
using WebApiService.Models;

namespace WebApiService.Controllers
{
    public class ItemMstController : ApiController
    {
        // GET:api/ItemMst
        public HttpResponseMessage Get()
        {
            try
            {
                var objGet = _CShopManagement.GetInstance.ItmViewBindBA();
                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(JArray.FromObject(objGet).ToString(), Encoding.UTF8, "application/Json")
                };
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
                
            }
        }

        // GET:api/ItemMst/5
        public string Get(int id)
        {
            return "value";
        }

        // POST:api/ItemMst
        public HttpResponseMessage Post([FromBody]Item_Mst_Class clsItem)
        {
            try
            {
                TBL_ITEM_MST objItem = new TBL_ITEM_MST();
                objItem.ITM_NAME = clsItem.ITM_NAME;
                objItem.ITM_DESC = clsItem.ITM_DESC;
                objItem.CREATED_BY = clsItem.CREATED_BY;
                objItem.CREATED_ON = clsItem.CREATED_ON;
                objItem.IS_ACTIVE = clsItem.IS_ACTIVE;
                objItem.IS_DELETE = clsItem.IS_DELETE;
                _CShopManagement.GetInstance.InsertItem(objItem);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        // PUT:api/ItemMst/5
        public HttpResponseMessage Put(int id, [FromBody]Item_Mst_Class clsItem)
        {
            try
            {
                if (clsItem.IS_ACTIVE == true)
                {
                    TBL_ITEM_MST objItem = new TBL_ITEM_MST();
                    objItem.ITM_NAME = clsItem.ITM_NAME;
                    objItem.ITM_DESC = clsItem.ITM_DESC;
                    objItem.MODIFIED_BY = clsItem.CREATED_BY;
                    objItem.MODIFIED_ON = clsItem.CREATED_ON;
                    objItem.IS_ACTIVE = clsItem.IS_ACTIVE;
                    objItem.IS_DELETE = clsItem.IS_DELETE;
                    _CShopManagement.GetInstance.UpdateItem(objItem, id);
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                else
                {
                    TBL_ITEM_MST objItem = new TBL_ITEM_MST();
                    objItem.MODIFIED_BY = clsItem.CREATED_BY;
                    objItem.MODIFIED_ON = clsItem.CREATED_ON;
                    objItem.IS_ACTIVE = clsItem.IS_ACTIVE;
                    objItem.IS_DELETE = clsItem.IS_DELETE;
                    _CShopManagement.GetInstance.DeleteItem(objItem,id);
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        // DELETE:api/ItemMst/5
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }
    }
}
